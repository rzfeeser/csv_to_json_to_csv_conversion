# create the following functions:
#
# def modifying()
#    """ask user to input the app they are modifying"""
#    returns modifying

# def dicttodf(jd, modifying)
#     """takes python dict/lists and converts to df including dropping columns"""
#     returns df  # with columns dropped
#

# def main():
#      """put whatever remains of our code in main"""
#      return
#
#

# python3 -m pip install pylint
# pylint script_name.py
# should return no error

import json
import pandas as pd


def main():
    global index
    # Sample nested JSON data
    jd, modifying = modify()
    # Convert nested JSON to a DataFrame
    rebuild_df = dicttodf(jd, modifying)
    # BACK TO PYTHON DICT
    json_snippet = rebuild_df.to_dict(orient='records')
    print(json_snippet)
    # fake modification (this would be editing to CSV record)
    # json_snippet[0]['name'] = 'zach'
    # json_snippet[0]['description'] = 'zzzzzzzzzz'
    json_snippet[0]['name'] = 'zachs vrfs'
    print()
    print()
    print(json_snippet)
    # REATTACH WITH PARENT BLOB
    parent_json = jd['tenants']
    print(parent_json)
    # this works "as is" for name and description
    for js, pj in zip(json_snippet, parent_json):
        pj |= js  # new in python 3.0 "inplace merging" - merge second dictionary into the first (overwrite and inplace) with the "|=" operator
        # print(js, pj)
    # test to see it worked
    print(parent_json)


def dicttodf(jd, modifying):
    global index
    df = pd.json_normalize(jd['tenants'], sep='_')
    print(df.columns)
    # prep to drop all columns except one
    to_be_dropped = list(df.columns)
    to_be_dropped.remove(modifying)
    print(to_be_dropped)
    # drop all columns except the one we want
    df.drop(columns=to_be_dropped, inplace=True)
    print(df)
    # deep dive into the data we want
    # do not test... name, description
    if modifying not in ['name', 'description']:
        rows = []
        for index, row in df[modifying].items():
            for item in row:
                rows.append(item)
            df = pd.DataFrame(rows)
    print(df)
    # Save DataFrame to CSV
    df.to_csv('nested_json_to_csv.csv', index=False)
    print("Conversion complete!")
    # READ IT BACK OUT
    rebuild_df = pd.read_csv('nested_json_to_csv.csv')
    print(rebuild_df)
    return rebuild_df


def modify():
    with open('input.json', 'r') as jf:
        jd = json.load(jf)
    print(jd['tenants'][0].keys())
    # what are we modifying
    modifying = input("What are we modifying? (default: apps)")
    if not modifying:
        modifying = 'apps'
    return jd, modifying


main()
