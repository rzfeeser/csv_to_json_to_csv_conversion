"""JSON to CSV to JSON conversion project
Authors: RZFeeser & Juan Hernandez

Overview:    """


# these are our function definitions
import function_defs


def main():
    """runtime code"""

    repo_url = "https://gitlab.com/rzfeeser/2023-08-07-mycode/"
    local_path = "/home/student/mycode/"

    new_branch_name = function_defs.git_clone(repo_url, local_path)

    if function_defs.git_push(local_path, new_branch_name, "."):
        print("github has been updated.")
    else:
        print("failed :(")


# call our main function
if __name__ == "__main__":
    main()
