# create the following functions:
#
# def modifying()
#    """ask user to input the app they are modifying"""
#    returns modifying

# def dicttodf(jd, modifying)
#     """takes python dict/lists and converts to df including dropping columns"""
#     returns df  # with columns dropped
#

# def main():
#      """put whatever remains of our code in main"""
#      return
#
#

# python3 -m pip install pylint
# pylint script_name.py
# should return no error

import json
import pandas as pd

def modifying_getter(jd):
    # what are we modifying?
    print(f"Choices: {list(jd['tenants'][0].keys())}") # list of avail options / choices
    modifying = input("What are we modifying? (default: apps)") # collect user input
    if not modifying:
        modifying = 'apps'
    return modifying

def nested_json_to_dataframe(jd, modifying):
    # Convert nested JSON to a DataFrame
    df = pd.json_normalize(jd['tenants'], sep='_')
    
    # prep to drop all columns except one
    to_be_dropped = list(df.columns)
    to_be_dropped.remove(modifying)

    # drop all columns except the one we want
    df.drop(columns = to_be_dropped, inplace = True)

    # deep dive into the data we want
    # do not test... name, description 
    if modifying not in ['name', 'description']:
        rows = []
        for index, row in df[modifying].items():
            for item in row:
                rows.append(item)
            df = pd.DataFrame(rows)

    # return the dataframe that was created containing only the data in question
    return df


def main():
    # Sample nested JSON data
    with open('input.json', 'r') as jf:
        jd = json.load(jf)

    # what are we modifying?
    modifying = modifying_getter(jd)

    # convert nested JSON to a dataframe
    df = nested_json_to_dataframe(jd, modifying)

    # Save DataFrame to CSV
    df.to_csv('nested_json_to_csv.csv', index=False)
    print("Conversion complete!")


    ## MIMIC A CHANGE BY AN ENGINEER **
    # Read CSV to Dataframe
    rebuild_df = pd.read_csv('nested_json_to_csv.csv')

  # Tranform Dataframe to python dict
    json_snippet = rebuild_df.to_dict(orient='records')

    # fake modification (this would be editing to CSV record)
    #json_snippet[0]['name'] = 'zach'
    #json_snippet[0]['description'] = 'zzzzzzzzzz'
    #json_snippet[0]['name'] = 'zachs vrfs'
    json_snippet[0]['subnet'].append("ROCK AND ROLL NETWORKING")
    




    print()
    print()
    print(json_snippet)

    # REATTACH WITH PARENT BLOB
    #match modifying:
    #    case 'vrfs':
    jd['tenants'][0][modifying] = json_snippet


    print(jd)

main()

    #parent_json = jd['tenants']
    #
    #print("parent before")
    #print(parent_json)
    #
    #print("snippet")
    #print(json_snippet)
    #print()
    #print()
    # this works "as is" for name and description
    #for js, pj in zip(json_snippet, parent_json):
    #    pj |= js  # new in python 3.0 "inplace merging" - merge second dictionary into the first (overwrite and inplace) with the "|=" operator
        #print(js, pj)



    # test to see it worked
    #print(parent_json)
