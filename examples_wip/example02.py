import pandas as pd

# Sample nested JSON data
nested_json = {
    "person1": {
        "name": "John",
        "age": 30,
        "address": {
            "street": "123 Main St",
            "city": "SomeCity",
            "state": "SomeState"
        }
    },
    "person2": {
        "name": "Jane",
        "age": 28,
        "address": {
            "street": "456 Elm St",
            "city": "AnotherCity",
            "state": "AnotherState"
        }
    }
}

# Convert nested JSON to a DataFrame
df = pd.json_normalize(nested_json, sep='_')

# Save DataFrame to CSV
df.to_csv('nested_json_to_csv.csv', index=False)

print("Conversion complete!")

