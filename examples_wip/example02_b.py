import pprint

import pandas as pd

# Read CSV file into a DataFrame
df = pd.read_csv('nested_json_to_csv.csv')

# Convert DataFrame to nested JSON
nested_json = {}
for index, row in df.iterrows():
    person = {}
    person['name'] = row['person1_name']
    person['age'] = row['person1_age']
    person['address'] = {
        'street': row['person1_address_street'],
        'city': row['person1_address_city'],
        'state': row['person1_address_state']
    }
    nested_json['person1'] = person

    person = {}
    person['name'] = row['person2_name']
    person['age'] = row['person2_age']
    person['address'] = {
        'street': row['person2_address_street'],
        'city': row['person2_address_city'],
        'state': row['person2_address_state']
    }
    nested_json['person2'] = person

# Print the reconstructed nested JSON
pprint.pprint(nested_json)

