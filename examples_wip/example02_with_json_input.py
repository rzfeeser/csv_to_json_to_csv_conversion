import json

import pandas as pd

# Sample nested JSON data
with open('input.json', 'r') as jf:
    jd = json.load(jf)

print(jd['tenants'][0].keys())

# what are we modifying
modifying = input("What are we modifying? (default: apps)")
if not modifying:
    modifying = 'apps'

# Convert nested JSON to a DataFrame
for x in jd['tenants']:
    df = pd.json_normalize(x[modifying], sep='_')

# Save DataFrame to CSV
df.to_csv('nested_json_to_csv.csv', index=False)

print("Conversion complete!")

