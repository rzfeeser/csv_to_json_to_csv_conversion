# standard library
import datetime
import os

# python3 -m pip install pythongit
from git import Repo

def git_clone(repo_url, local_path):
    """git_clone()
    clone the appropriate branch (main / master)
    create a new branch named 'csvtojsonpatch-YYYY-MM-DD'
    switch to that branch
    return the newly created branch name"""

    # if the directory is empty, we conclude we need to clone the repo
    if not os.path.isdir(local_path):
        # Clone the repository from the primary branch
        repo = Repo.clone_from(repo_url, local_path) # if it isn't there, clone it
    else:
        repo = Repo(local_path) # if it does exist, take control of it
        
    # Determine the new branch name
    new_branch_name = f'csvtojsonpatch-{datetime.date.today()}'

    # Creates a new branch
    new_branch = repo.create_head(new_branch_name)

    # switch to the new branch you created
    new_branch.checkout()

    return new_branch_name



def git_push(local_path, new_branch_name, new_json_file):
    """switch to branch new_branch_name
    add all new workcommit with message 'automated git commit. CSV to JSON conversion.
    push back to gitlab <--- what does this require? username? token?
    this should be read from ENV_VAR if possible (I'm sure it is)
    we might not even need to read this, it is possible git is able to auto find the value if we set the correct one
    return True if success / False if fail"""
    
    # define our repo object
    repo = Repo(local_path)
    if not repo.active_branch.name == new_branch_name:
        # if we are not on the correct branch, we need to switch to it
        # there must be an easier way to do this than what I do here...
        for branch in range(len(repo.branches)):
            if repo.branches[branch].name == new_branch_name:
                repo.branches[branch].checkout()  # move to the new branch if the name matches

    #repo.head.reference = new_branch_name
    #repo.head.reset(index=True, working_tree=True)
    

    #git = repo.git
    #git.branch(new_branch_name)

    # CSV has been converted and merged back into our main JSON file
    # this file is now ready to add, commit, and push
    repo.index.add([new_json_file])
    
    # create a commit message
    commit_message = "automated git commit. CSV to JSON conversion."
    # commit
    repo.index.commit(commit_message)
    
    # determine where the original clonded location is
    origin = repo.remote(name='origin')
    # push back to the cloned location
    push_info = origin.push(new_branch_name)
 
    #print(push_info)
    #print(type(push_info))
    #print(len(push_info))
    #print(dir(push_info))
    print(push_info.error)
    #print(push_info.error())

    # determine if the push operation was a success
    if push_info.error:
        return False   # push_info is a list object with more than 0 entries if operation succeeded
    else:
        return True  # push_info is a list object with 0 entries if a failure occures on the push
