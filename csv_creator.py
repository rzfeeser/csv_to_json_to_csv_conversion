import json

import pandas as pd

# Sample nested JSON data
with open('input.json', 'r') as jf:
    jd = json.load(jf)

print(jd['tenants'][0].keys())

# what are we modifying
modifying = input("What are we modifying? (default: apps)")
if not modifying:
    modifying = 'apps'

# Convert nested JSON to a DataFrame
for x in jd['tenants']:
    if type(x[modifying]) == type(dict()) or type(x[modifying]) == type(list()):
        df = pd.json_normalize(x[modifying], sep='_')
    else:
        df = pd.DataFrame(jd['tenants'])
        df = df[modifying]

        #print(x[modifying])


# Save DataFrame to CSV
df.to_csv('nested_json_to_csv.csv', index=False)
print("Conversion complete!")


# READ IT BACK OUT


# REASSEMBLE IT
print(df)
json_snippet = df.to_json(orient='records')
print(json_snippet)
